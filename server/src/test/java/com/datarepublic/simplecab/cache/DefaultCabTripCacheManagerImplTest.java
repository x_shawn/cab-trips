package com.datarepublic.simplecab.cache;

import com.datarepublic.simplecab.pojo.CabTrip;
import com.google.common.collect.ImmutableList;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.List;

import static com.datarepublic.simplecab.util.DateTimeUtil.endOfNewYorkToday;
import static com.datarepublic.simplecab.util.DateTimeUtil.startOfNewYorkToday;
import static com.google.common.collect.Lists.newArrayList;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.util.Collections.EMPTY_LIST;
import static org.junit.Assert.*;

public class DefaultCabTripCacheManagerImplTest {

    /* mocking medallions */
    private static final String BLANK_MEDALLION = "  ";
    private static final String EXISTING_MEDALLION = "whatever existing medallion";

    /* mocking pickup date range in the past */
    private static final ZonedDateTime START_OF_YESTERDAY = startOfNewYorkToday().minusDays(1);
    private static final ZonedDateTime END_OF_YESTERDAY = endOfNewYorkToday().minusDays(1);

    private CabTripCacheManager cabTripCacheManager;

    @Before
    public void populateInitialTestDataInCacheBeforeEachTestCase() {
        cabTripCacheManager = new DefaultCabTripCacheManagerImpl();

        CabTrip toBeCached = new CabTrip();
        toBeCached.setMedallion(EXISTING_MEDALLION);
        toBeCached.setPickupDateTime(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY));

        cabTripCacheManager.save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY, newArrayList(toBeCached));
    }

    @After
    public void clearAllTestDataAfterEachTestCase() {
        cabTripCacheManager.clear();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptBlankMedallion() {
        cabTripCacheManager.getCabTripsBy(BLANK_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPickupStart() {
        cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, null, END_OF_YESTERDAY);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPickupEnd() {
        cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowInvalidPickupDateTimeRange() {
        cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, endOfNewYorkToday(), startOfNewYorkToday());
    }

    @Test
    public void shouldReturnTrueWhenLookingUpCachedCabTrip() {
        assertTrue(cabTripCacheManager.hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY));
    }

    @Test
    public void shouldReturnFalseWhenLookingUpNonCachedCabTrip() {
        assertFalse(cabTripCacheManager.hasAnyCabTrip(EXISTING_MEDALLION, startOfNewYorkToday(), endOfNewYorkToday()));
    }

    @Test
    public void shouldReturnCachedCabTrip() {
        List<CabTrip> cachedTrips = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY,
                END_OF_YESTERDAY);

        assertEquals(1, cachedTrips.size());
        assertEquals(EXISTING_MEDALLION, cachedTrips.get(0).getMedallion());
        assertEquals(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY), cachedTrips.get(0).getPickupDateTime());
    }

    @Test
    public void shouldReturnEmptyListOfCabTripsWhenNotCached() {
        List<CabTrip> cachedTrips = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, startOfNewYorkToday(),
                endOfNewYorkToday());

        assertTrue(cachedTrips.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotSaveEmptyCabTripsIntoCache() {
        cabTripCacheManager.save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY, EMPTY_LIST);
    }

    @Test
    public void shouldAddNewCabTripIntoExistingCacheWhenItsPickUpDateTimeFallsIntoSameRange() {
        // BEFORE: the state change in cache
        List<CabTrip> existingCache = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY,
                END_OF_YESTERDAY);

        assertEquals(1, existingCache.size());
        assertEquals(EXISTING_MEDALLION, existingCache.get(0).getMedallion());
        assertEquals(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY), existingCache.get(0).getPickupDateTime());

        // AFTER: the state change - i.e. a new trip within the same pickup datetime range being cached
        CabTrip newTrip = new CabTrip();
        newTrip.setMedallion(EXISTING_MEDALLION);
        newTrip.setPickupDateTime(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY));
        cabTripCacheManager.save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY, ImmutableList.of(newTrip));

        // THEN: verify if the new trip being saved into the existing cache
        existingCache = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        assertEquals(2, existingCache.size());
        assertEquals(newTrip, existingCache.get(1));

        // THEN: verify if the earlier cached trip still exists
        assertEquals(EXISTING_MEDALLION, existingCache.get(0).getMedallion());
        assertEquals(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY), existingCache.get(0).getPickupDateTime());
    }

    @Test
    public void shouldSaveNewCabTripIntoNewCacheWhenItsPickUpDateTimeFallsOutOfExistingRange() {
        // GIVEN: a new trip whose pickup datetime is out of range
        CabTrip newTrip = new CabTrip();
        newTrip.setMedallion(EXISTING_MEDALLION);
        newTrip.setPickupDateTime(ISO_ZONED_DATE_TIME.format(endOfNewYorkToday()));

        // WHEN: saving the above new trip into cache
        cabTripCacheManager.save(EXISTING_MEDALLION, startOfNewYorkToday(), endOfNewYorkToday(),
                ImmutableList.of(newTrip));

        // THEN: verify if the above new trip being cached
        List<CabTrip> newCache = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, startOfNewYorkToday(),
                endOfNewYorkToday());
        assertEquals(1, newCache.size());
        assertEquals(newTrip, newCache.get(0));

        // THEN: verify if the earlier cached trip still exists
        List<CabTrip> earlierCache = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY,
                END_OF_YESTERDAY);
        assertEquals(1, earlierCache.size());
        assertEquals(EXISTING_MEDALLION, earlierCache.get(0).getMedallion());
        assertEquals(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY), earlierCache.get(0).getPickupDateTime());
    }

    @Test
    public void shouldRemoveAllCachedCabTrips() {
        // BEFORE: clearing the cache
        List<CabTrip> existingCache = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY,
                END_OF_YESTERDAY);
        assertEquals(1, existingCache.size());

        // AFTER: clearing the cache
        cabTripCacheManager.clear();
        existingCache = cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        assertTrue(existingCache.isEmpty());
    }
}
