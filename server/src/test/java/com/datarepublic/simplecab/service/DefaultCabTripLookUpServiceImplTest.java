package com.datarepublic.simplecab.service;

import com.datarepublic.simplecab.cache.CabTripCacheManager;
import com.datarepublic.simplecab.pojo.CabTrip;
import com.datarepublic.simplecab.repository.CabTripDAO;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

import static com.datarepublic.simplecab.util.DateTimeUtil.*;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.EMPTY_SET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DefaultCabTripLookUpServiceImplTest {

    /* mocking existing medallions */
    private static final String EXISTING_MEDALLION = "whatever existing medallion";
    private static final Set<String> NON_EMPTY_MEDALLIONS = ImmutableSet.of(EXISTING_MEDALLION);

    /* mocking pickup date range in the past */
    private static final ZonedDateTime START_OF_YESTERDAY = startOfNewYorkToday().minusDays(1);
    private static final ZonedDateTime END_OF_YESTERDAY = endOfNewYorkToday().minusDays(1);

    /* mocking signal of bypassing cache */
    private static final boolean FRESH_CAB_TRIPS_WANTED = true;
    private static final boolean NOT_NECESSARY_BE_FRESH = false;

    @Mock
    private CabTripCacheManager cabTripCacheManager;

    @Mock
    private CabTripDAO cabTripDAO;

    @InjectMocks
    private DefaultCabTripLookUpServiceImpl cabTripLookUpService;

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptEmptyMedallions() {
        cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(EMPTY_SET,
                startOfNewYorkToday(), endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPickupStartDate() {
        cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                null, endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPickupEndDate() {
        cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                startOfNewYorkToday(), null, NOT_NECESSARY_BE_FRESH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowInvalidPickupDateRange() {
        cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                endOfNewYorkToday(), startOfNewYorkToday(), NOT_NECESSARY_BE_FRESH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAllowPickupDateRangeStartFromFuture() {
        cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                startOfNewYorkTomorrow(), endOfNewYorkTomorrow(), NOT_NECESSARY_BE_FRESH);
    }

    @Test
    public void shouldLookUpCabTripsFromDBDirectlyGivenTheFreshDataWantedSignal() {
        // GIVEN: those cab trips saved in both cache and DB
        List<CabTrip> savedInCacheAndDB = ImmutableList.of(new CabTrip());
        when(cabTripCacheManager.hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY))
                .thenReturn(true);
        when(cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY))
                .thenReturn(savedInCacheAndDB);
        when(cabTripDAO.findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY))).thenReturn(savedInCacheAndDB);

        // WHEN: always looking up the fresh cab trips
        List<CabTrip> trips = cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
                NON_EMPTY_MEDALLIONS, START_OF_YESTERDAY, END_OF_YESTERDAY, FRESH_CAB_TRIPS_WANTED);

        // THEN: verify if the above trips found
        assertEquals(savedInCacheAndDB, trips);

        // THEN: verify if the above trips found from DB only and skip cache
        verify(cabTripDAO).findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY));
        verify(cabTripCacheManager, never()).hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripCacheManager, never()).getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripCacheManager, never()).save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY,
                savedInCacheAndDB);
    }

    @Test
    public void shouldLookUpCabTripsFromDBDirectlyGivenPickupDateRangeAcrossToday() {
        // GIVEN: those cab trips happened today and saved in DB only, as they might keep happening during the day
        List<CabTrip> happenedTodayAndSavedInDBOnly = ImmutableList.of(new CabTrip());
        when(cabTripDAO.findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()),
                timestampInUTCFrom(endOfNewYorkToday()))).thenReturn(happenedTodayAndSavedInDBOnly);

        // WHEN: looking up cab trips happening today
        List<CabTrip> trips = cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
                NON_EMPTY_MEDALLIONS, startOfNewYorkToday(), endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH);

        // THEN: verify if the above trips found from DB
        assertEquals(happenedTodayAndSavedInDBOnly, trips);

        // THEN: verify if the above trips found via DAO directly and skip cache, as today's trips would keep increasing
        verify(cabTripDAO).findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()),
                timestampInUTCFrom(endOfNewYorkToday()));
        verify(cabTripCacheManager, never()).hasAnyCabTrip(EXISTING_MEDALLION, startOfNewYorkToday(),
                endOfNewYorkToday());
        verify(cabTripCacheManager, never()).getCabTripsBy(EXISTING_MEDALLION, startOfNewYorkToday(),
                endOfNewYorkToday());
        verify(cabTripCacheManager, never()).save(EXISTING_MEDALLION, startOfNewYorkToday(), endOfNewYorkToday(),
                happenedTodayAndSavedInDBOnly);
    }


    @Test
    public void shouldLookUpCabTripsFromCacheFirstGivenPickupDateRangeInThePast() {
        // GIVEN: those cab trips happened in the past and saved in cache
        CabTrip happenedInThePastAndSavedInCache = mock(CabTrip.class);
        when(cabTripCacheManager.hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY))
                .thenReturn(true);
        when(cabTripCacheManager.getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY))
                .thenReturn(ImmutableList.of(happenedInThePastAndSavedInCache));

        // WHEN: looking up cab trips in the past
        List<CabTrip> trips = cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
                NON_EMPTY_MEDALLIONS, START_OF_YESTERDAY, END_OF_YESTERDAY, NOT_NECESSARY_BE_FRESH);

        // THEN: verify if the above trips found from Cache
        assertEquals(1, trips.size());
        assertEquals(happenedInThePastAndSavedInCache, trips.get(0));

        // THEN: verify if the above trips found via Cache and skip DB, as historic trips existed in cache as mocked
        verify(cabTripCacheManager).hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripCacheManager).getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripDAO, never()).findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY));
        verify(cabTripCacheManager, never()).save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY,
                ImmutableList.of(happenedInThePastAndSavedInCache));
    }

    @Test
    public void shouldKeepFindingCabTripsFromDBGivenPickupDateRangeInThePastAndNotFoundInCache() {
        // GIVEN: those cab trips not saved in cache; but in DB
        CabTrip happenedInThePastAndSavedInDBOnly = mock(CabTrip.class);
        when(happenedInThePastAndSavedInDBOnly.getMedallion()).thenReturn(EXISTING_MEDALLION);
        when(happenedInThePastAndSavedInDBOnly.getPickupDateTime())
                .thenReturn(ISO_ZONED_DATE_TIME.format(START_OF_YESTERDAY));

        when(cabTripCacheManager.hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY))
                .thenReturn(false);
        when(cabTripDAO.findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY))).thenReturn(ImmutableList.of(happenedInThePastAndSavedInDBOnly));

        // WHEN: looking up cab trips in the past
        List<CabTrip> trips = cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
                NON_EMPTY_MEDALLIONS, START_OF_YESTERDAY, END_OF_YESTERDAY, NOT_NECESSARY_BE_FRESH);

        // THEN: verify if the above trips found from DB
        assertEquals(1, trips.size());
        assertEquals(happenedInThePastAndSavedInDBOnly, trips.get(0));

        // THEN: verify if we did try to lookup the above trips in cache first, and didn't find anything
        verify(cabTripCacheManager).hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripCacheManager, never()).getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);

        // THEN: verify if we keep looking in DB and updating cache accordingly when found
        verify(cabTripDAO).findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY));
        verify(cabTripCacheManager).save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY, ImmutableList.of(
                happenedInThePastAndSavedInDBOnly));
    }

    @Test
    public void shouldReturnEmptyListGivenPickupDateRangeInThePastAndNothingFoundFromEitherCacheOrDAO() {
        // GIVEN: nothing found from cache; nor from DB
        when(cabTripCacheManager.hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY))
                .thenReturn(false);
        when(cabTripDAO.findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY))).thenReturn(EMPTY_LIST);

        // WHEN: looking up cab trips in the past
        List<CabTrip> trips = cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
                NON_EMPTY_MEDALLIONS, START_OF_YESTERDAY, END_OF_YESTERDAY, NOT_NECESSARY_BE_FRESH);

        // THEN: verify if nothing found
        assertTrue(trips.isEmpty());

        // THEN: verify if we did try to lookup the above trips in cache first; then keep looking in DB
        verify(cabTripCacheManager).hasAnyCabTrip(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripCacheManager, never()).getCabTripsBy(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY);
        verify(cabTripDAO).findManyBy(NON_EMPTY_MEDALLIONS, timestampInUTCFrom(START_OF_YESTERDAY),
                timestampInUTCFrom(END_OF_YESTERDAY));
        verify(cabTripCacheManager, never()).save(EXISTING_MEDALLION, START_OF_YESTERDAY, END_OF_YESTERDAY, EMPTY_LIST);
    }
}
