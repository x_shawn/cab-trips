package com.datarepublic.simplecab.repository;

import com.datarepublic.simplecab.pojo.CabTrip;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;
import java.util.Set;

import static com.datarepublic.simplecab.util.DateTimeUtil.*;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.EMPTY_MAP;
import static java.util.Collections.EMPTY_SET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.booleanThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultCabTripDAOImplTest {

    /* mocking medallions */
    private static final Set<String> EMPTY_MEDALLIONS = EMPTY_SET;
    private static final String EXISTING_MEDALLION = "whatever existing medallions";
    private static final Set<String> EXISTING_MEDALLIONS = ImmutableSet.of(EXISTING_MEDALLION);

    /* mocking cab trip */
    private static final CabTrip EXISTING_CAB_TRIP_IN_DB = new CabTrip();

    @Mock
    private NamedParameterJdbcOperations jdbcOperations;

    @InjectMocks
    private DefaultCabTripDAOImpl cabTripDAO;

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptEmptyMedallions() {
        cabTripDAO.findManyBy(EMPTY_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()),
                timestampInUTCFrom(endOfNewYorkToday()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPickupStartTimestamp() {
        cabTripDAO.findManyBy(EXISTING_MEDALLIONS, null, timestampInUTCFrom(endOfNewYorkToday()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptNullPickupEndTimestamp() {
        cabTripDAO.findManyBy(EXISTING_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptInvalidPickupTimestampRange() {
        cabTripDAO.findManyBy(EMPTY_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()),
                timestampInUTCFrom(endOfNewYorkToday()));
    }

    @Test
    public void shouldReturnEmptyListOfCabTripsGivenNothingFoundFromDB() {
        // GIVEN: nothing found in DB
        when(jdbcOperations.query(anyString(), any(SqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(EMPTY_LIST);

        // WHEN: trying to find any cab trip from DB
        List<CabTrip> tripsFound = cabTripDAO.findManyBy(EXISTING_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()),
                timestampInUTCFrom(endOfNewYorkToday()));

        // THEN: verify if nothing found
        assertTrue(tripsFound.isEmpty());

        // THEN: verify if we did the query as mocked
        verify(jdbcOperations).query(anyString(), any(SqlParameterSource.class), any(RowMapper.class));
    }

    @Test
    public void shouldReturnNonEmptyListOfCabTripsWhenFoundFromDB() {
        // GIVEN: an existing cab trip found in DB
        when(jdbcOperations.query(anyString(), any(SqlParameterSource.class), any(RowMapper.class)))
                .thenReturn(ImmutableList.of(EXISTING_CAB_TRIP_IN_DB));

        // WHEN: trying to find any cab trip from DB
        List<CabTrip> tripsFound = cabTripDAO.findManyBy(EXISTING_MEDALLIONS, timestampInUTCFrom(startOfNewYorkToday()),
                timestampInUTCFrom(endOfNewYorkToday()));

        // THEN: verify if the existing cab trip found
        assertEquals(1, tripsFound.size());
        assertEquals(EXISTING_CAB_TRIP_IN_DB, tripsFound.get(0));

        // THEN: verify if we did the query as mocked
        verify(jdbcOperations).query(anyString(), any(SqlParameterSource.class), any(RowMapper.class));
    }

    @Test
    public void shouldReturnFalseGivenNothingFoundFromDB() {
        // GIVEN: nothing found in DB
        JdbcOperations basicOperations = mock(JdbcOperations.class);
        when(basicOperations.queryForMap(anyString())).thenReturn(EMPTY_MAP);
        when(jdbcOperations.getJdbcOperations()).thenReturn(basicOperations);

        // THEN: verify the state change - i.e. no data in DB
        assertFalse(cabTripDAO.anyData());

        // THEN: verify the above state change is due to the following mock interactions
        verify(basicOperations).queryForMap(anyString());
        verify(jdbcOperations).getJdbcOperations();
    }

    @Test
    public void shouldReturnTrueGivenSomethingFoundFromDB() {
        // GIVEN: something found in DB
        JdbcOperations basicOperations = mock(JdbcOperations.class);
        when(basicOperations.queryForMap(anyString())).thenReturn(ImmutableMap.of(
                "medallion", EXISTING_MEDALLION
        ));
        when(jdbcOperations.getJdbcOperations()).thenReturn(basicOperations);

        // THEN: verify the state change - i.e. data does exist in DB
        assertTrue(cabTripDAO.anyData());

        // THEN: verify the above state change is due to the following mock interactions
        verify(basicOperations).queryForMap(anyString());
        verify(jdbcOperations).getJdbcOperations();
    }
}
