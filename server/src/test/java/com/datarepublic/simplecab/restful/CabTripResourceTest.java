package com.datarepublic.simplecab.restful;

import com.datarepublic.simplecab.cache.CabTripCacheManager;
import com.datarepublic.simplecab.pojo.CabTrip;
import com.datarepublic.simplecab.pojo.CabTripRequest;
import com.datarepublic.simplecab.service.CabTripLookUpService;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;

import static com.datarepublic.simplecab.util.DateTimeUtil.*;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE_TIME;
import static java.util.Collections.EMPTY_LIST;
import static java.util.Collections.EMPTY_SET;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class CabTripResourceTest {

    /* mocking medallions */
    private static final Set<String> EMPTY_MEDALLIONS = EMPTY_SET;
    private static final Set<String> NON_EMPTY_MEDALLIONS = ImmutableSet.of("whatever non-empty medallions");

    /* mocking pickup day in New York */
    private static final String ILLEGAL_PICKUP_DATE_FORMAT = ISO_LOCAL_DATE_TIME.format(endOfNewYorkToday());
    private static final String NEW_YORK_TODAY = ISO_LOCAL_DATE.format(startOfNewYorkToday());
    private static final String NEW_YORK_TOMORROW = ISO_LOCAL_DATE.format(startOfNewYorkTomorrow());

    /* mocking cab trips found */
    private static final List<CabTrip> CAB_TRIPS_FOUND = ImmutableList.of(new CabTrip());

    /* mocking signal of bypassing cache lookup */
    private static final boolean NOT_NECESSARY_BE_FRESH = false;

    @Mock
    private CabTripLookUpService cabTripLookUpService;

    @Mock
    private CabTripCacheManager cabTripCacheManager;

    @InjectMocks
    private CabTripResource cabTripResource;

    @Test(expected = NullPointerException.class)
    public void shouldNotAcceptNullRequest() {
        cabTripResource.auditNewYorkCabTripsPerMedallionsAndPickupDate(null);
    }

    @Test
    public void shouldRespond400BadRequestGivenEmptyMedallions() {
        // GIVEN: a CabTripRequest with empty medallions
        CabTripRequest request = mock(CabTripRequest.class);
        when(request.getMedallions()).thenReturn(EMPTY_MEDALLIONS);

        // WHEN: trying to audit cab trips via the above request
        ResponseEntity response = cabTripResource.auditNewYorkCabTripsPerMedallionsAndPickupDate(request);

        // THEN: verify the state change - i.e. BAD_REQUEST response
        assertEquals(BAD_REQUEST, response.getStatusCode());

        // THEN: verify the following mock interactions that caused the above state change
        verify(request).getMedallions();
    }

    @Test
    public void shouldRespond400BadRequestGivenIllegalPickupDate() {
        verifyIfRespond400BadRequestGivenInvalidPickupDateOf(ILLEGAL_PICKUP_DATE_FORMAT);
    }

    @Test
    public void shouldRespond400BadRequestGivenFuturePickupDate() {
        verifyIfRespond400BadRequestGivenInvalidPickupDateOf(NEW_YORK_TOMORROW);
    }

    @Test
    public void shouldRespond404NotFoundGivenNoCabTripsInfoFoundFromLookupService() {
        // GIVEN: a CabTripRequest with non-empty medallions and Today of New York as pickup date
        CabTripRequest request = mock(CabTripRequest.class);
        when(request.getMedallions()).thenReturn(NON_EMPTY_MEDALLIONS);
        when(request.getPickupDateInNewYork()).thenReturn(NEW_YORK_TODAY);
        when(request.isFreshWanted()).thenReturn(NOT_NECESSARY_BE_FRESH);

        // GIVEN: no cab trips can be found
        when(cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                startOfNewYorkToday(), endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH)).thenReturn(EMPTY_LIST);

        // WHEN: trying to audit cab trips via the above request
        ResponseEntity response = cabTripResource.auditNewYorkCabTripsPerMedallionsAndPickupDate(request);

        // THEN: verify the state change - i.e. NOT_FOUND response
        assertEquals(NOT_FOUND, response.getStatusCode());

        // THEN: verify the following mock interactions that caused the above state change
        verify(request).getMedallions();
        verify(request).getPickupDateInNewYork();
        verify(cabTripLookUpService).lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                startOfNewYorkToday(), endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH);
    }

    @Test
    public void shouldRespond200OKWithCabTripsInfoFoundFromLookupService() {
        // GIVEN: a CabTripRequest with non-empty medallions and Today of New York as pickup date
        CabTripRequest request = mock(CabTripRequest.class);
        when(request.getMedallions()).thenReturn(NON_EMPTY_MEDALLIONS);
        when(request.getPickupDateInNewYork()).thenReturn(NEW_YORK_TODAY);
        when(request.isFreshWanted()).thenReturn(NOT_NECESSARY_BE_FRESH);

        // GIVEN: cab trips found
        when(cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                startOfNewYorkToday(), endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH)).thenReturn(CAB_TRIPS_FOUND);

        // WHEN: trying to audit cab trips via the above request
        ResponseEntity response = cabTripResource.auditNewYorkCabTripsPerMedallionsAndPickupDate(request);

        // THEN: verify the state change - i.e. OK response with the found cab trips as body
        assertEquals(OK, response.getStatusCode());
        assertEquals(CAB_TRIPS_FOUND, response.getBody());

        // THEN: verify the following mock interactions that caused the above state change
        verify(request).getMedallions();
        verify(request).getPickupDateInNewYork();
        verify(cabTripLookUpService).lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(NON_EMPTY_MEDALLIONS,
                startOfNewYorkToday(), endOfNewYorkToday(), NOT_NECESSARY_BE_FRESH);
    }

    @Test
    public void shouldAskCacheManagerToDoTheClearingWorkAsRequired() {
        // WHEN: being required to clean the cache
        ResponseEntity response = cabTripResource.clearCache();

        // THEN: verify if no content in response
        assertEquals(NO_CONTENT, response.getStatusCode());

        // THEN: verify if the cache manager being asked to do the actual clearing work
        verify(cabTripCacheManager).clear();
    }

    private void verifyIfRespond400BadRequestGivenInvalidPickupDateOf(String invalidPickupDate) {
        // GIVEN: a CabTripRequest with non-empty medallions and an invalid pickup date in New York
        CabTripRequest request = mock(CabTripRequest.class);
        when(request.getMedallions()).thenReturn(NON_EMPTY_MEDALLIONS);
        when(request.getPickupDateInNewYork()).thenReturn(invalidPickupDate);

        // WHEN: trying to audit cab trips via the above request
        ResponseEntity response = cabTripResource.auditNewYorkCabTripsPerMedallionsAndPickupDate(request);

        // THEN: verify the state change - i.e. BAD_REQUEST response
        assertEquals(BAD_REQUEST, response.getStatusCode());

        // THEN: verify the following mock interactions that caused the above state change
        verify(request).getMedallions();
        verify(request).getPickupDateInNewYork();
    }
}
