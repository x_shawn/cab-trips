package com.datarepublic.simplecab.monitor;

import com.datarepublic.simplecab.repository.CabTripDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.actuate.health.Health;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.boot.actuate.health.Status.DOWN;
import static org.springframework.boot.actuate.health.Status.UP;

@RunWith(MockitoJUnitRunner.class)
public class HealthCheckTest {

    @Mock
    private CabTripDAO cabTripDAO;

    @InjectMocks
    private HealthCheck healthCheck;

    @Test
    public void shouldBringServerDownGivenNothingInDB() {
        // GIVEN: no data at all in DB, which is abnormal
        when(cabTripDAO.anyData()).thenReturn(false);

        // WHEN: checking server health condition
        Health condition = healthCheck.health();

        // THEN: verify if it is DOWN, due to no data
        assertEquals(DOWN, condition.getStatus());
        verify(cabTripDAO).anyData();
    }

    @Test
    public void shouldBringServerAppGivenSomethingInDB() {
        // GIVEN: some data in DB
        when(cabTripDAO.anyData()).thenReturn(true);

        // WHEN: checking server health condition
        Health condition = healthCheck.health();

        // THEN: verify if it is UP, due to data found
        assertEquals(UP, condition.getStatus());
        verify(cabTripDAO).anyData();
    }
}
