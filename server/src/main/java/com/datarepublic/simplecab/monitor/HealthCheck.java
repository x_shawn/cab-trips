package com.datarepublic.simplecab.monitor;

import com.datarepublic.simplecab.repository.CabTripDAO;
import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;

/**
 * A health check facility validating if current server is UP or DOWN, by checking the DB state (i.e. connection and data)
 *
 * <p>
 *     NOTE: as per micro-service best practice, health check is part of service monitoring process and should be integrated
 *     into its infrastructure utilities.
 * </p>
 *
 * @author Shawn
 */
public class HealthCheck extends AbstractHealthIndicator {
    private CabTripDAO cabTripDAO;

    public HealthCheck(CabTripDAO cabTripDAO) {
        this.cabTripDAO = cabTripDAO;
    }

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        if (cabTripDAO.anyData()) {
            builder.up();
        } else {
            builder.down();
        }
    }
}
