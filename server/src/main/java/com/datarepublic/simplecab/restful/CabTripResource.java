package com.datarepublic.simplecab.restful;

import com.datarepublic.simplecab.cache.CabTripCacheManager;
import com.datarepublic.simplecab.pojo.CabTrip;
import com.datarepublic.simplecab.pojo.CabTripRequest;
import com.datarepublic.simplecab.pojo.ErrorResponse;
import com.datarepublic.simplecab.service.CabTripLookUpService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Set;

import static com.datarepublic.simplecab.util.DateTimeUtil.AMERICA_NEW_YORK;
import static com.datarepublic.simplecab.util.DateTimeUtil.startOfNewYorkToday;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.*;
import static org.springframework.util.CollectionUtils.isEmpty;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * {@link CabTrip} resource request/response handler.
 *
 * @author Shawn
 */
@RestController
public class CabTripResource {
    private CabTripLookUpService cabTripLookUpService;
    private CabTripCacheManager cabTripCacheManager;

    public CabTripResource(CabTripLookUpService cabTripLookUpService, CabTripCacheManager cabTripCacheManager) {
        this.cabTripLookUpService = cabTripLookUpService;
        this.cabTripCacheManager = cabTripCacheManager;
    }

    /**
     * RESTful API auditing New York cab trips, per medallions and pickup date.
     *
     * <p>
     *     NOTE: API interface uses POST instead of GET verb, due to the cab trips could keep updating during the day,
     *     if the pickup day is today; as such current API is not idempotent (i.e. same request being sent multiple
     *     times would not result in same response); therefore POST is chosen over GET to avoid the wrong cache at
     *     client end.
     * </p>
     *
     * @param request the de-serialized PoJo encapsulating cab trips query parameters, e.g. a set of unique medallions,
     *                pickup date in New York (and DATE ONLY), etc.
     *
     * @return BAD_REQUEST response if the request contains illegal query parameters, e.g. malformed pickup date; or
     * NOT_FOUND response if no cab trips can be found as per current request; or OK response with found cab trips
     */
    @RequestMapping(value = "/new_york", method = POST, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity auditNewYorkCabTripsPerMedallionsAndPickupDate(@RequestBody CabTripRequest request) {
        requireNonNull(request);

        // basic request field validation: NULL or EMPTY check
        Set<String> medallions = request.getMedallions();
        String pickupDateStrVal = request.getPickupDateInNewYork();
        if (isEmpty(medallions) || isBlank(pickupDateStrVal)) {
            return badRequest().body(new ErrorResponse(request,
                    "Neither medallions nor pickup date can be empty !"));
        }

        /*
        * datetime validation and process on the following grounds:
        *
        * 1. format check: DATE ONLY
        * 2. content check: future not allowed, as no trips would ever happen
        * 3. 'American/New_York' timezone supplement for the later service and caching use
        */
        LocalDate pickupDate;
        try {
            pickupDate = LocalDate.parse(pickupDateStrVal);
        } catch (DateTimeParseException ex) {
            return badRequest().body(new ErrorResponse(request,
                    "The given pickup date string format [" + ex.getParsedString() +
                    "] is illegal; please correct it to ISO_LOCAL_DATE format, i.e. yyyy-MM-dd"));
        }
        if (pickupDate.isAfter(startOfNewYorkToday().toLocalDate())) {
            return badRequest().body(new ErrorResponse(request,
                    "Pickup date cannot be any future day of New York !"));
        }
        ZonedDateTime pickupStart = pickupDate.atStartOfDay(AMERICA_NEW_YORK);
        ZonedDateTime pickupEnd = pickupStart.plusDays(1).minusSeconds(1);

        // find cab trips of New York via lookup service
        List<CabTrip> found = cabTripLookUpService.lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(medallions,
                pickupStart, pickupEnd, request.isFreshWanted());

        return isEmpty(found) ? notFound().build() : ok(found);
    }

    /**
     * Handy endpoint facilitating admin client to clear cab trips cache.
     *
     * <p>
     *     NOTE: this is NOT a 'RESTful' API exposing or manipulating any resource; a RESTful resource is a certain
     *     entity (or the result of a certain operation) with a certain schema.
     *
     *     As such, {@link CabTrip} is the representation of our RESTful resource that we want to expose to the public;
     *     either the cache or DB is our internal processing strategy that cannot and should not be a RESTful resource
     *     exposed publicly.
     *
     *     Therefore, to avoid any confusion, here is just a handy Servlet endpoint to facilitate admin to clear the
     *     cache, and this endpoint needs to be secured and used internally only in the future.
     * </p>
     *
     * @return NO_CONTENT response
     */
    @RequestMapping(value = "/cache_removal", method = POST)
    public ResponseEntity clearCache() {
        cabTripCacheManager.clear();
        return noContent().build();
    }
}
