package com.datarepublic.simplecab.service;

import com.datarepublic.simplecab.pojo.CabTrip;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;

/**
 * A lookup service finding cab trips.
 *
 * @author Shawn
 */
public interface CabTripLookUpService {

    /**
     * Look up New York cab trips from either cache or DB, given a set of unique medallions and a pickup datetime range.
     *
     * <p>
     *     NOTE: We always look up cab trips from DB directly in the following cases:
     *
     *     <ul>
     *         <li>
     *             <code>freshCabTripsWanted</code> flag is set
     *         </li>
     *         <li>
     *             the given pickup datetime range is across TODAY in New York, due to the cab trips could keep happening
     *             during the current day
     *         </li>
     *     </ul>
     *
     *     When we look up the historic cab trips, we follow this sequence: cache lookup -> DB query -> cache update
     * </p>
     *
     * @param medallions a set of unique medallions
     * @param pickupStart the starting point (inclusive) of pickup datetime range
     * @param pickupEnd the ending point (inclusive) of pickup datetime range
     * @param freshCabTripsWanted a signal of bypassing cache to query DB directly
     *
     * @return a list of {@link CabTrip} found from either cache or DB; an empty list if nothing found from either source
     */
    List<CabTrip> lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
            Set<String> medallions, ZonedDateTime pickupStart, ZonedDateTime pickupEnd, boolean freshCabTripsWanted);
}
