package com.datarepublic.simplecab.service;

import com.datarepublic.simplecab.cache.CabTripCacheManager;
import com.datarepublic.simplecab.pojo.CabTrip;
import com.datarepublic.simplecab.repository.CabTripDAO;

import java.time.ZonedDateTime;
import java.util.*;

import static com.datarepublic.simplecab.util.DateTimeUtil.*;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Default implementor of {@link CabTripLookUpService}
 *
 * @author Shawn
 */
public class DefaultCabTripLookUpServiceImpl implements CabTripLookUpService {
    private CabTripCacheManager cabTripCacheManager;
    private CabTripDAO cabTripDAO;

    public DefaultCabTripLookUpServiceImpl(CabTripCacheManager cabTripCacheManager, CabTripDAO cabTripDAO) {
        this.cabTripCacheManager = cabTripCacheManager;
        this.cabTripDAO = cabTripDAO;
    }

    @Override
    public List<CabTrip> lookUpNewYorkCabTripsByMedallionsAndPickupDateTimeRange(
            Set<String> medallions, ZonedDateTime start, ZonedDateTime end, boolean freshCabTripsWanted) {

        checkArgument(!isEmpty(medallions) && nonNull(start) && nonNull(end) && !start.isAfter(end) &&
                start.isBefore(startOfNewYorkTomorrow()));

        /*
        * we always fetch the data from DB directly and skip cache, only when:
        *
        * 1. 'freshCabTripsWanted' flag is set true by caller; or
        *
        * 2. trips happened today and would probably continue happening during the day; in this case, if we save them
        * from DB into Cache, the next time when caller fetches the today's trips from cache, the amount could be different,
        * as new trips had happened and been saved into DB; therefore, we only follow the strategy - i.e.
        * cache lookup -> DB query -> cache update, for the historic trips
        */
        if (freshCabTripsWanted || end.isAfter(startOfNewYorkToday())) {
            return cabTripDAO.findManyBy(medallions, timestampInUTCFrom(start), timestampInUTCFrom(end));
        }

        // when pickup happened in the past: follow strategy - i.e. cache lookup -> DB query -> cache update
        List<CabTrip> found = new ArrayList<>(medallions.size());
        Set<String> noTripsInCache = new HashSet<>();

        // collect cab trips from cache and track those medallions who have no trips in cache
        for (String medallion : medallions) {
            if (cabTripCacheManager.hasAnyCabTrip(medallion, start, end)) {
                found.addAll(cabTripCacheManager.getCabTripsBy(medallion, start, end));
            } else {
                noTripsInCache.add(medallion);
            }
        }

        // continue collecting those trips that do not exist in cache but might exist in DB; then update cache accordingly
        if (!noTripsInCache.isEmpty()) {
            List<CabTrip> fromDB = cabTripDAO.findManyBy(noTripsInCache, timestampInUTCFrom(start), timestampInUTCFrom(end));
            if (!isEmpty(fromDB)) {
                found.addAll(fromDB);
                updateCacheWithTripsFound(fromDB, start, end);
            }
        }

        return found;
    }

    private void updateCacheWithTripsFound(List<CabTrip> fromDB, ZonedDateTime start, ZonedDateTime end) {
        Map<String, List<CabTrip>> tripsPerMedallion = new HashMap<>();

        for (CabTrip trip : fromDB) {
            String medallion = trip.getMedallion();
            tripsPerMedallion.putIfAbsent(medallion, new ArrayList<>());

            ZonedDateTime pickupDate = ZonedDateTime.parse(trip.getPickupDateTime());
            if (pickupDate.isEqual(start) || pickupDate.isEqual(end) ||
                    pickupDate.isAfter(start) && pickupDate.isBefore(end)) {
                tripsPerMedallion.get(medallion).add(trip);
            }
        }

        tripsPerMedallion.forEach((medallion, trips) -> cabTripCacheManager.save(medallion, start, end, trips));
    }
}
