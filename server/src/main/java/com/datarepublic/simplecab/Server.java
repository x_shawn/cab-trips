package com.datarepublic.simplecab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@ComponentScan({ "com.datarepublic.simplecab.restful", "com.datarepublic.simplecab.config" })
@EnableSwagger2
public class Server {

    public static void main(String[] args){
        SpringApplication.run(Server.class, args);
    }
}
