package com.datarepublic.simplecab.config;

import com.datarepublic.simplecab.cache.CabTripCacheManager;
import com.datarepublic.simplecab.cache.DefaultCabTripCacheManagerImpl;
import com.datarepublic.simplecab.monitor.HealthCheck;
import com.datarepublic.simplecab.repository.CabTripDAO;
import com.datarepublic.simplecab.repository.DefaultCabTripDAOImpl;
import com.datarepublic.simplecab.service.CabTripLookUpService;
import com.datarepublic.simplecab.service.DefaultCabTripLookUpServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.time.LocalDate;

/**
 * Server-wide context and config. e.g. Spring beans.
 *
 * @author Shawn
 */
@Configuration
public class ServerConfig {

    @Autowired
    private NamedParameterJdbcOperations jdbcOperations;

    @Bean
    public CabTripDAO cabTripDAO() {
        return new DefaultCabTripDAOImpl(jdbcOperations);
    }

    @Bean
    public CabTripCacheManager cabTripCacheManager() {
        return new DefaultCabTripCacheManagerImpl();
    }

    @Bean
    public CabTripLookUpService cabTripLookUpService() {
        return new DefaultCabTripLookUpServiceImpl(cabTripCacheManager(), cabTripDAO());
    }

    @Bean
    public HealthIndicator healthIndicator() {
        return new HealthCheck(cabTripDAO());
    }

    /**
     * Swagger UI config.
     *
     * @return a {@link Docket} instance
     */
    @Bean
    public Docket configApiDoc() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("New York Cab Trips API")
                .description("RESTful API auditing and exposing the information of cab trips in New York")
                .version("1.0")
                .license("DataRepublic " + LocalDate.now().getYear())
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}
