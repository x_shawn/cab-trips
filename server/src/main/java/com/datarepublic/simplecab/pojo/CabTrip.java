package com.datarepublic.simplecab.pojo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * PoJO encapsulating the information of a particular cab trip.
 *
 * NOTE: for the sake of simplicity in current PoC (Proof-of-Concept) project, this PoJo serves as both domain-specific
 * entity used in the O-R (Object-Relation) mapping process, and DTO used in the serialization process as RESTful
 * response body.
 *
 * @author Shawn
 */
public class CabTrip {
    private String medallion;

    private String hackLicense;
    private String vendorId;
    private Integer rateCode;
    private String storeFwdFlag;

    /* datetime in string value whose format follows ISO_ZONED_DATE_TIME */
    private String pickupDateTime;
    private String dropoffDatetime;

    private Integer passengerCount;
    private Integer tripTimeInSecs;

    /* BigDecimal type instead of floating type to ensure the precision */
    private BigDecimal tripDistance;

    public String getMedallion() {
        return medallion;
    }

    public void setMedallion(String medallion) {
        this.medallion = medallion;
    }

    public void setHackLicense(String hackLicense) {
        this.hackLicense = hackLicense;
    }

    public String getHackLicense() {
        return hackLicense;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setRateCode(Integer rateCode) {
        this.rateCode = rateCode;
    }

    public Integer getRateCode() {
        return rateCode;
    }

    public void setStoreFwdFlag(String storeFwdFlag) {
        this.storeFwdFlag = storeFwdFlag;
    }

    public String getStoreFwdFlag() {
        return storeFwdFlag;
    }

    public String getPickupDateTime() {
        return pickupDateTime;
    }

    public void setPickupDateTime(String pickupDateTime) {
        this.pickupDateTime = pickupDateTime;
    }

    public void setDropoffDatetime(String dropoffDatetime) {
        this.dropoffDatetime = dropoffDatetime;
    }

    public String getDropoffDatetime() {
        return dropoffDatetime;
    }

    public void setPassengerCount(Integer passengerCount) {
        this.passengerCount = passengerCount;
    }

    public Integer getPassengerCount() {
        return passengerCount;
    }

    public void setTripTimeInSecs(Integer tripTimeInSecs) {
        this.tripTimeInSecs = tripTimeInSecs;
    }

    public Integer getTripTimeInSecs() {
        return tripTimeInSecs;
    }

    public void setTripDistance(BigDecimal tripDistance) {
        this.tripDistance = tripDistance;
    }

    public BigDecimal getTripDistance() {
        return tripDistance;
    }
}
