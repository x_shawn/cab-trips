package com.datarepublic.simplecab.pojo;

/**
 * PoJo encapsulating the API interaction context, when an error occurs, e.g. the original request, error message, etc.
 *
 * @author Shawn
 */
public class ErrorResponse {
    private CabTripRequest request;
    private String errorMessage;

    public ErrorResponse(CabTripRequest request, String errorMessage) {
        this.request = request;
        this.errorMessage = errorMessage;
    }

    public CabTripRequest getRequest() {
        return request;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
