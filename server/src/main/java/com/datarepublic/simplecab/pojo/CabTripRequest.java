package com.datarepublic.simplecab.pojo;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * PoJo encapsulating cab trip API request, e.g. a set of unique medallions, pickup date, etc.
 *
 * @author Shawn
 */
public class CabTripRequest {
    private Set<String> medallions;
    private String pickupDateInNewYork;
    private boolean freshWanted;

    public CabTripRequest() {
        medallions = new LinkedHashSet<>();
        freshWanted = false;
    }

    public Set<String> getMedallions() {
        return medallions;
    }

    public void setMedallions(Set<String> medallions) {
        this.medallions = medallions;
    }

    public String getPickupDateInNewYork() {
        return pickupDateInNewYork;
    }

    public void setPickupDateInNewYork(String pickupDateInNewYork) {
        this.pickupDateInNewYork = pickupDateInNewYork;
    }

    public boolean isFreshWanted() {
        return freshWanted;
    }

    public void setFreshWanted(boolean freshWanted) {
        this.freshWanted = freshWanted;
    }
}
