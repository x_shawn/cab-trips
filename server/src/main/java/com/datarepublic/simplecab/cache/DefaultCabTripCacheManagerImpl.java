package com.datarepublic.simplecab.cache;

import com.datarepublic.simplecab.pojo.CabTrip;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.nonNull;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Default implementor of {@link CabTripCacheManager}
 *
 * @author Shawn
 */
public class DefaultCabTripCacheManagerImpl implements CabTripCacheManager {

    /*
    * Our internal cache using ConcurrentMap data structure whose key is a tuple { medallion, [pickupStart, pickupEnd] }
    * and whose value is a list of cab trips happened within a certain pickup datetime range for a particular cab medallion
    *
    * NOTE: choosing Java ConcurrentMap is for the sake of simplicity in current PoC project; in the future, it should
    * be replaced by more mature caching solution, e.g. EhCache or Google Guava Cache, etc.
    */
    private static final ConcurrentMap<CacheKey, List<CabTrip>> CACHE = new ConcurrentHashMap();

    @Override
    public boolean hasAnyCabTrip(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd) {
        checkArgument(isNotBlank(medallion) && nonNull(pickupStart) && nonNull(pickupEnd) &&
                !pickupStart.isAfter(pickupEnd));

        return CACHE.containsKey(new CacheKey(medallion, pickupStart, pickupEnd));
    }

    @Override
    public List<CabTrip> getCabTripsBy(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd) {
        return hasAnyCabTrip(medallion, pickupStart, pickupEnd) ?
                CACHE.get(new CacheKey(medallion, pickupStart, pickupEnd)) : new ArrayList<>();
    }

    @Override
    public void save(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd, List<CabTrip> foundFromDB) {
        checkArgument(!isEmpty(foundFromDB));

        CacheKey cacheKey = new CacheKey(medallion, pickupStart, pickupEnd);

        if (hasAnyCabTrip(medallion, pickupStart, pickupEnd)) {
            // no multi-threaded contention, due to always adding new trips
            CACHE.get(cacheKey).addAll(foundFromDB);
        } else {
            // atomic put operation guaranteed by ConcurrentMap
            CACHE.putIfAbsent(cacheKey, foundFromDB);
        }
    }

    @Override
    public void clear() {

        /*
        * Due to the Atomicity of Writes, e.g. PUT, REMOVE, and CLEAR methods, in ConcurrentHashMap is only guaranteed at
        * hash bucket (i.e. implemented as its internal Binary Tree Node) level, instead of the whole Map level,
        * we need to explicitly ensure the consistent cache data view between its Reads and Writes, when we clear the
        * whole Map; otherwise, we would end up some clients still fetch the cached data while we are clearing it
        * hash bucket by bucket.
        */
        synchronized (CACHE) {
            CACHE.clear();
        }
    }

    /*
    * Cache key used internally, which consists of a particular medallion and a pickup datetime range.
    */
    private static final class CacheKey {
        private final String medallion;
        private final ZonedDateTime pickupStart;
        private final ZonedDateTime pickupEnd;

        private CacheKey(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd) {
            this.medallion = medallion;
            this.pickupStart = pickupStart;
            this.pickupEnd = pickupEnd;
        }

        @Override
        public boolean equals(Object another) {
            if (this == another) return true;
            if (another == null || getClass() != another.getClass()) return false;

            CacheKey cacheKey = (CacheKey) another;
            return Objects.equals(medallion, cacheKey.medallion) &&
                    Objects.equals(pickupStart, cacheKey.pickupStart) &&
                    Objects.equals(pickupEnd, cacheKey.pickupEnd);
        }

        @Override
        public int hashCode() {
            return Objects.hash(medallion, pickupStart, pickupEnd);
        }
    }
}
