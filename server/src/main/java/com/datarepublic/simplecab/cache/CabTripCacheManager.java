package com.datarepublic.simplecab.cache;

import com.datarepublic.simplecab.pojo.CabTrip;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * A facility caching the historic cab trips to minimize DB round trips for better performance.
 *
 * @author Shawn
 */
public interface CabTripCacheManager {

    /**
     * Inspect if any cab trip exists in cache, given a particular medallion and a pickup datetime range.
     *
     * @param medallion a unique medallion representing a cab
     * @param pickupStart the starting point (inclusive) of pickup datetime range
     * @param pickupEnd the ending point (inclusive) of pickup datetime range
     *
     * @return TRUE if any cab trip found; otherwise FALSE
     */
    boolean hasAnyCabTrip(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd);

    /**
     * Fetch a list of historic cab trips from cache, given a particular medallion and a pickup datetime range.
     *
     * @param medallion a unique medallion representing a cab
     * @param pickupStart the starting point (inclusive) of pickup datetime range
     * @param pickupEnd the ending point (inclusive) of pickup datetime range
     *
     * @return a list of {@link CabTrip} found from cache; an empty list when nothing found
     */
    List<CabTrip> getCabTripsBy(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd);

    /**
     * Save a list of cab trips found from DB into cache, given a particular medallion and a pickup datetime range.
     *
     * <P>
     *     NOTE: by saving, the new list of cab trips are either added into the existing cache, if the given medallion
     *     and pickup datetime range have already been registered before; otherwise added into a new cache.
     * </P>
     *
     * @param medallion a unique medallion representing a cab
     * @param pickupStart the starting point (inclusive) of pickup datetime range
     * @param pickupEnd the ending point (inclusive) of pickup datetime range
     * @param foundFromDB a list of {@link CabTrip} found from DB
     */
    void save(String medallion, ZonedDateTime pickupStart, ZonedDateTime pickupEnd, List<CabTrip> foundFromDB);

    /**
     * Clear the cache.
     *
     * <p>
     *     NOTE: the clearing operation would block any other cache operations, e.g. fetch and save, which are happening
     *     at the same time, to ensure the data view consistency.
     * </p>
     */
    void clear();
}
