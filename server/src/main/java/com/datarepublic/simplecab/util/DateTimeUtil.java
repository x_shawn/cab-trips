package com.datarepublic.simplecab.util;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDate.now;
import static java.time.ZoneOffset.UTC;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.util.Objects.isNull;

/**
 * A datetime manipulation utility providing often-used services, e.g. converting a zoned datetime into timestamp in DB,
 * and vice versa, etc.
 *
 * @author Shawn
 */
public final class DateTimeUtil {
    public static final ZoneId AMERICA_NEW_YORK = ZoneId.of("America/New_York");

    private DateTimeUtil() {
    }

    public static ZonedDateTime startOfNewYorkToday() {
        return now(AMERICA_NEW_YORK).atStartOfDay(AMERICA_NEW_YORK);
    }

    public static ZonedDateTime endOfNewYorkToday() {
        return startOfNewYorkToday().plusDays(1).minusSeconds(1);
    }

    public static ZonedDateTime startOfNewYorkTomorrow() {
        return startOfNewYorkToday().plusDays(1);
    }

    public static ZonedDateTime endOfNewYorkTomorrow() {
        return endOfNewYorkToday().plusDays(1);
    }

    /**
     * Convert a datetime in New York into a timestamp in UTC for DAO use.
     *
     * @param dateTimeOfNewYork a datetime at 'America/New_York' zone
     * @return a timestamp in UTC
     */
    public static Timestamp timestampInUTCFrom(ZonedDateTime dateTimeOfNewYork) {
        if (isNull(dateTimeOfNewYork)) return null;

        LocalDateTime toUTC = dateTimeOfNewYork.withZoneSameInstant(UTC).toLocalDateTime();
        return Timestamp.valueOf(toUTC);
    }

    /**
     * Convert a UTC timestamp from DB to a datetime in New York for Service use.
     *
     * @param utcTimestamp a timestamp in UTC
     * @return a datetime at 'America/New_York' zone
     */
    public static ZonedDateTime fromUTCToET(Timestamp utcTimestamp) {
        // time without any timezone info
        LocalDateTime withoutTimeZone = utcTimestamp.toLocalDateTime();

        // supplement 'GMT/Z' time zone as the base time zone for UTC time
        ZonedDateTime dateTimeInUTC = ZonedDateTime.of(withoutTimeZone, UTC);

        return dateTimeInUTC.withZoneSameInstant(AMERICA_NEW_YORK);
    }
}
