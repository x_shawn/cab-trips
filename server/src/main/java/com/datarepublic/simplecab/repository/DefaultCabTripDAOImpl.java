package com.datarepublic.simplecab.repository;

import com.datarepublic.simplecab.pojo.CabTrip;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;

import java.sql.Timestamp;
import java.time.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.datarepublic.simplecab.util.DateTimeUtil.fromUTCToET;
import static com.google.common.base.Preconditions.checkArgument;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;
import static java.util.Objects.nonNull;
import static org.springframework.util.CollectionUtils.isEmpty;

/**
 * Default implementor of {@link CabTripDAO}
 *
 * @author Shawn
 */
public class DefaultCabTripDAOImpl implements CabTripDAO {
    private static final String CAB_TRIP_SQL = "SELECT * FROM cab_trip_data " +
            "WHERE pickup_datetime >= :pickupStart AND pickup_datetime <= :pickupEnd AND medallion in (:medallions)";

    private static final String DATA_INSPECT_SQL = "SELECT medallion FROM cab_trip_data LIMIT 1";

    private NamedParameterJdbcOperations jdbcOperations;

    public DefaultCabTripDAOImpl(NamedParameterJdbcOperations jdbcOperations) {
        this.jdbcOperations = jdbcOperations;
    }

    @Override
    public List<CabTrip> findManyBy(Set<String> medallions, Timestamp pickupStart, Timestamp pickupEnd) {
        checkArgument(!isEmpty(medallions) && nonNull(pickupStart) && nonNull(pickupEnd) &&
                !pickupStart.after(pickupEnd));

        // prepare query parameters
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("pickupStart", pickupStart);
        parameters.addValue("pickupEnd", pickupEnd);
        parameters.addValue("medallions", new ArrayList<>(medallions));

        // do the query and O-R mapping
        return jdbcOperations.query(CAB_TRIP_SQL, parameters, (resultSet, rowNum) -> {
            CabTrip mapped = new CabTrip();

            mapped.setMedallion(resultSet.getString("medallion"));
            mapped.setHackLicense(resultSet.getString("hack_license"));
            mapped.setVendorId(resultSet.getString("vendor_id"));
            mapped.setRateCode(resultSet.getInt("rate_code"));
            mapped.setStoreFwdFlag(resultSet.getString("store_and_fwd_flag"));

            // convert UTC timestamp into ET zoned datetime for later service use
            ZonedDateTime pickupDateTimeInNewYork = fromUTCToET(resultSet.getTimestamp("pickup_datetime"));
            mapped.setPickupDateTime(ISO_ZONED_DATE_TIME.format(pickupDateTimeInNewYork));
            ZonedDateTime dropoffDateTimeInNewYork = fromUTCToET(resultSet.getTimestamp("dropoff_datetime"));
            mapped.setDropoffDatetime(ISO_ZONED_DATE_TIME.format(dropoffDateTimeInNewYork));

            mapped.setPassengerCount(resultSet.getInt("passenger_count"));
            mapped.setTripTimeInSecs(resultSet.getInt("trip_time_in_secs"));
            mapped.setTripDistance(resultSet.getBigDecimal("trip_distance"));

            return mapped;
        });
    }

    @Override
    public boolean anyData() {
        JdbcOperations basicOperations = jdbcOperations.getJdbcOperations();
        return !isEmpty(basicOperations.queryForMap(DATA_INSPECT_SQL));
    }
}
