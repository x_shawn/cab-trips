package com.datarepublic.simplecab.repository;

import com.datarepublic.simplecab.pojo.CabTrip;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

/**
 * A Data Access Object querying database and handling O-R mapping.
 *
 * @author Shawn
 */
public interface CabTripDAO {

    /**
     * Find many cab trips, given a set of unique medallions and a pickup datetime range.
     *
     * @param medallions a set of unique cab medallions
     * @param pickupStart the starting point (inclusive) of pickup datetime range
     * @param pickupEnd the ending point (inclusive) of pickup datetime range
     *
     * @return a list of {@link CabTrip} found; otherwise an empty list
     */
    List<CabTrip> findManyBy(Set<String> medallions, Timestamp pickupStart, Timestamp pickupEnd);

    /**
     * Inspect if any data existing in current DB.
     *
     * @return TRUE if data found; otherwise FALSE
     */
    boolean anyData();
}
