# NYCT-API: New York Cab Trip API
This README document aims to walk you through my personal solution - i.e. **NYCT-API**, to the coding challenge from DataRepublic.

# Requirement
When I go through the requirement description on [Simple Cab Exercise](http://datarepublic.litmos.com/course/1312601/module/3718680?moduletoken=tPMZxiuGQbeVze0SaoP2NkvY0AtpIZuZWz1lRc25ZHffAbS84V8X3AIYZUxUU93N&LPId=0) web page, as well as the initial project structure, I'm not 100% sure about the requirement scope. For instance, the web page seems to only ask for a RESTful API at server end while the initial project structure consists of both `Client` and `Server`modules. Also, regarding the API, the web page firstly defines the data schema for the required RESTful resource; yet later it specifies the API response to return the amount of cab trips found.

With the above confusion, I've re-defined the requirement statement as follows in my best understanding.

 1. Task Scope: to realise two APIs in `Server`module only

 2. Feature Scope: one RESTful API exposing New York cab trips information for the public use; one Servlet endpoint allowing the internal admin client to clear the cache

You may wonder why I use two different terms - i.e. RESTful API and Servlet endpoint, to define the requirement. I'm glad you've asked, because this has bothered me during the requirement clarification in my mind. IMHO (in-my-humble-opinion), a public RESTful API aims to expose resource information with certain schema; meanwhile its internal mechanism, e.g. cache, should not and cannot be defined as a RESTful resource; nor should be exposed externally. To make the second piece of requirement - i.e. allowing the client to clear the cache, a valid feature requirement, the only use case I can imagine is to facilitate the internal admin user to manually operate the API. As such, it is more of a Servlet operation, and less of a RESTful API, which leads to the above 2 different terms.

# Solution
To provide a proof of concept (**PoC**) solution, I follow the well-known **KISS** (keep-it-simple-stupid) principle and *focus on the minimum requirement only* (re-defined as above). As a result, the current version of my solution has taken a few assumptions, which I would elaborate on later.

## Build & Run
Before jumping into the solution *design & implementation* details, here let me firstly explain how to *build & run* **NYCT-API** application on your local machine. As such, you can have a *look & feel* on this toy prior to its engineering details.

### Pre-requisite
To play around my ugly toy, you firstly need to ensure the following to be installed  onto your local environment.

 - **Java 8+**
 - **Maven 3+**
 - **MySQL 5+**

Once the above has been installed, you then need to create a MySQL database instance and populate it via the dump file attached onto [Simple Cab Exercise](http://datarepublic.litmos.com/course/1312601/module/3718680?moduletoken=tPMZxiuGQbeVze0SaoP2NkvY0AtpIZuZWz1lRc25ZHffAbS84V8X3AIYZUxUU93N&LPId=0) page.

Then you can fetch the source code project via this [google drive share link](https://drive.google.com/file/d/1-0rk08vp3XGSOcQgo9V0eLZmUarR4Cms/view?usp=sharing); or `git clone` it from [its bitbucket repository](https://bitbucket.org/x_shawn/cab-trips/src/master/)

When you are under the `server` sub-directory of the fetched source code project, open your *Terminal* app if you are on Mac OS X or any other Unix-alike OS; or *CMD* window if you are using Windows (and finger-crossing ...). After that, take a deep breath, then

### Just Run

    mvn spring-boot:run

If the server app boot-up had failed, double check if your local MySQL connection is consistent with the configuration in `application.properties`, e.g. the database instance name in `spring.datasource.url` property, the connection username and password, etc.

If you had still got MySQL datasource related error, kill both Java and MySQL processes and start it over. There is a [known issue about MySQL JDBC driver with Tomcat 8.x used by Spring Boot](https://github.com/spring-cloud/spring-cloud-netflix/issues/106).

If you want to go through the traditional build pipeline, including both Unit Test and Package steps. You can firstly

### Build

mvn clean install

To pass the Unit Test and package the whole app as a FAT JAR; then

### Run

java -jar target/server-1.0-SNAPSHOT.jar

### Try-out
To try out **NYCT-API** application locally, you need to construct a **HTTP POST request** as required by the following:

 - **header**: `Content-Type: application/json` and `Accept: application/json`
 - **body**: a JSON message with 3 field, e.g. 
`{
    "medallions": [
        "1354F44EAEDF475B489A1E5D972FF9AC",
        "2FBF9DAD51F548A6F1CE63165CC30BA7",
        "516034632743F243A53CD2B07C1B59CD"
    ],
    "pickupDateInNewYork": "2013-12-31",
    "freshWanted": false
}` 

Then send it to **endpoint**: [http://localhost:8000/cab_trips/new_york](http://localhost:8000/cab_trips/new_york).

**NOTE**: if you don't have any REST client, e.g. *postman* or *curl*, you can still try it out via our swagger UI - i.e. [http://localhost:8000/cab_trips/swagger-ui.html](http://localhost:8000/cab_trips/swagger-ui.html) on your web browser.

**NOTE 2**: if neither app boot-up nor API try-out works, ...hmmmm... call me ?

## Design
First of all, thanks for reading on ! after trying out my ugly toy (and I hope it is working on your local)...

Here I would like to detail the solution design via both Class Model and Operation Sequence.

### Class Model
By adopting the classic multi-layer design approach, **NYCT-API** consists of the following layers

 - **POJOs**: entities here represent the API input and output - e.g. `CabTripRequest` and `CabTrip`. For the sake of simplicity, here we mix both domain-specific entity and DTO concepts in the same PoJo layer.

 - **Repository**: entities here are Data Access Objects (**DAO**) who handle the database query and Object-Relation (**O-R**) mapping.

 - **Cache**: entities here cache the historic cab trips PoJo to minimize DB round trips for better performance.

 - **Service**: entities here determine the cab trips lookup strategy, e.g. look up cache prior to DB query

 - **Resource**: RESTful resource request & response handler

Each layer as above has its own concern, e.g. **Repository** focus on the DB access while **Resource** focus on the API interaction via REST/HTTP protocol. Their dependencies are the reverse order as above, e.g. **Resource** -> **Service** -> **Repository**.

### Operation Sequence
To put the above entities together, we can see how they work with each other via the following sequence diagram.

```mermaid
sequenceDiagram
CabTripResource ->> CabTripResource: is valid request ?
CabTripResource ->> CabTripResource: no -> BAD_REQUEST
CabTripResource ->> CabTripService: yes -> do lookup
CabTripService ->> CabTripService: bypass cache signal OR pickup date is today ?
CabTripService ->> CabTripDAO: yes -> query DB directly
CabTripService -->> CabTripCache: no -> existing cab trips in cache ?
CabTripCache ->> CabTripCache: get by key (medallion + pickup datetime range)
CabTripService ->> CabTripDAO: no -> query DB
CabTripService -->> CabTripCache: update cache with cab trips found in DB
CabTripCache ->> CabTripCache: save into existing cache or add into a new one
```

As you can see from the above operation sequence diagram, the essence of logic is how `Service` coordinates `DAO` and `Cache` when looking up cab trips. And all seems straightforward by following the classic strategy - i.e. `Cache Lookup` first; then `Database Query`; then `Cache Update`.

Yet, if you pay a close attention to the following sequence

> bypass cache signal OR pickup date is today ? -> yes -> query DB directly

You may wonder why we skip cache lookup and query DB directly, when the given pickup date is TODAY. Well, that is because during the day, the cab trips would keep happening. If we cache today's trips after the initial request, then later when the same request comes in, we return the cached result; yet during that 'later' period, new cab trips had happened and been recorded in DB, which had caused the data inconsistency.

To avoid that, we always query DB to get today's latest trips and only cache the historic ones. As such, we have actually taken an assumption - i.e. the historic data in DB would not get updated; thus it is safe to keep it in cache. Then that raises a new question - i.e. is this assumption valid ? Well I would say it depends on the actual business and supporting system. In a production environment, it should be in most cases; yet we can't eliminate the case where data migration or recovery being scheduled, which is often seen in real-life development. Yet back to **KISS** pinciple and **PoC** nature of this project, we've taken this assumption to guide later implementation.

## Implementation

To implement the above design, I firstly choose the following.

### Development Stack

 - **Spring Boot 1.5**: as a Java based micro-service development framework
 - **Maven 3.x**: as project building tool, which is suggested by sample code

With the above development facilities, I do want to highlight and share some of my thinking during the implementation.

### Some Highlights

 - **Date-time process in Java**

> To process date, time, timezone in Java, Oh, my ...

yeah, I know ... yet, to defend Java as a Java Dev, I reckon it is fair to say that for any language, processing date time quite often is a pain in the butt.

Here in our project, there are 2 specific cases where we need to be careful about the date time processing.

1. Date-time **De-serialization** and Serialziation during API interaction

As per the requirement, we need to accept a DATE and DATE ONLY as pickup date of cab trips in New York. And our application (at least local app) is running at `Australia/Sydney` zone. So if we define the `pickupDate` field of our `CabTripRequest` PoJo as `java.util.Date` type, the de-serialization process would instantiate this field by supplementing the default system timezone; yet the requester actually expects it to be `America/New_York` zoned date time. In this case, we can't further change the timezone after de-serialization, because that would change the date and time value.

To avoid the above, we have 2 choices: 
1). ask the client to pass the pickup date time with `America/New_York` timezone in a proper format, e.g. `ISO_ZONED_DATE_TIME`. Yet, this violates the initial requirement - i.e. DATE only; also it puts too much work on client side.

2). define the pickup date field as `java.lang.String` type and give it a proper name, i.e. `pickupDateInNewYork` to avoid confusion; then we explicitly convert the string value into `java.time.LocalDate` to retain its date and time value; then supplement it with `America/New_York` timezone. In doing so, we can not only process date, time, and timezone fragments separately and correctly; but also validate the request input and give a proper error message when failed.

Apparently, we go for the second option.

2. Cab trips **query in DB and O-R mapping**

When we populate our local MySQL database with the given dump file, we need to pay attention to the `datetime` type of `pickup_datetime` column, as well as the following dumping config

> SET TIME_ZONE='+00:00'

the above schema and dumping config indicates: `pickup_datetime`  value in DB is `UTC` time, which in turns suggest our app to do the datetime conversion between `UTC` and `ET` zones. Particularly, we need to convert our `America/New_York` zoned pickup date time into `UTC` zoned one when doing DB query; then vice versa when doing O-R mapping.

 - **POST** or **GET**

While you are going through my code, you may wonder why the RESTful API interface has been defined as using **POST** HTTP verb instead of **GET**. As per the requirement, aren't we just fetching and exposing the cab trips info without creating or updating anything ?

Well, we indeed only do the fetching work without updating or creating any info. Yet, due to the real-time data fetching from DB when the given pickup date is TODAY, the response could be different when the same request repeats itself multiple times, which means **our API is NOT idempotent**. As per REST and HTTP 1.1 guideline, GET verb is idempotent and the same request should result in the same response; as such, the client can further design its own caching mechanism while interaction with a GET verb based API. Therefore, to follow this guideline, POST is preferable over GET in our API interface.

 - **HealthCheck**

**As a self-contained service, it should be deployment and release friendly**. This means we should bear **service monitoring and measurement** in mind while we are developing a public RESTful API or micro-service, even though the main focus is functionality.

In our case, to ensure our API work, we need to ensure our database connection is established and there should be some existing data in DB (in other words, how can we expose cab trips info without any data in DB). This data existence validation should be part of our health check from application perspective, on top of the infrastructure-level heartbeat check. Thanks to Spring boot framework, it is easy to develop and the access is: [http://localhost:8000/cab_trips/health](http://localhost:8000/cab_trips/health)

## Test

**Ideally**, I should have done all levels of testing, including unit test, function test, load test, etc. 

**In reality**, given the time limit and PoC nature, I've **provided as much coverage as possible of unit test** as part of my TDD development. Additionally, I've **done some manual function test** after implementation. Given no deployment and release need, I **skip the load test**.

# Future Work

I really appreciate your patience, if you are still reading on here ! In terms of the future work, I would like to consider the following to improve each layer of our app

 - **DB** and **DAO**

As discussed earlier, we've dealt with the timezone issue due to the UTC time in DB and app could be potentially hosted anywhere. Other that time, we also have a potential location issue. If we've taken a close look at the database table schema, we do have extra `pickup_longitude`, `pickup_latitude`, `dropoff_longitude`, and `dropoff_latitude` columns defined as `double` type, even though those extra fields are not exposed by our API. Yet those columns might indicate our DB could have cab trips that do not belong to New York. Actually the initial requirement description never says, their DB only contains New York cab trips.

As such, ideally, we need to take the location into account when querying New York cab trips in DB, e.g. adding more `WHERE` `SQL`clauses to see if either the pickup location or drop-off location is New York. I did say **ideally**, right ? that is because the location type is not set properly if we do need to manipulate it. Up to MySQL 5, we can define GeoLocation specific type, e.g. `Point`, to encapsulate the coordinates instead of `double` type. In doing so, we can further utilise some out-of-box functions in MySQL to compare the location, calculate the distance, etc. And we all know, comparing the floating number is not reliable enough.

Furthermore, we should index `medallion` field for better query performance. Without indexing it, the time complexity of query is O(N) (N is the amount of DB records after pickup date time filtering); after indexing it, the time complexity becomes O(k) (k is the size of `medallion` query parameters, which is often much smaller than N).

Last but not least, we should consider clustering our DB. As indicated by the original requirement description, the data amount can be quite large. If our DB infrastructure is a single node, we can imagine the slow query as the data amount grows, e.g. query those cab trips within a year and happened 10 years ago. Through partitioning our DB into a clustered environment, the query can be dispatched properly into a proper DB node with smaller data amount for a better performance.

 - **Cache**

By following **KISS** principle, I've implemented it via `Java ConcurrentMap` and provided a `clear` method to manually clear the cache. Yet, we can't simply rely on the manual clearing work; instead, we should consider automating it based on certain strategies, e.g. auto-remove those least-recently-used (LRU) cab trips in cache based on the elapsed time; or auto-remove the oldest trips when the given cache limit is met.

Other than the auto-clearing requirement in future, the performance also needs to be taken into account. `Java ConcurrentMap` indeed is much faster than `Synchronized Map` due to its multi-threaded contention handling at hash bucket / entry level instead of the whole map, as well as its segmented hash table implementation. Yet, the default segments are 16, which means once we have more than 16 threads accessing the shared cache, its performance would be no better than `Synchronized Map`, not to mention that the `clear` method is implemented as a blocking method to ensure the data consistency, which would make performance worse.

To resolve either of the above concerns, we should adopt more mature caching solution, e.g. `EhCache` or `Google Guava Cache`.

 - **API**

Back to the very beginning when I explained the reason why I got bothered by the ambiguous meaning of `API` in the original requirment statement, and later used 2 different terms to re-define the `API` requirement, this sounds like a trivial naming or wording issue (well, I've been told naming issue is one of the biggest issue in computer science though ...lol...). Yet it does reflect the case where we need to be cautious about what we want to expose externally and internally, and put different security mechanism on each of them.

Regarding our RESTful API exposing the New York cab trips for the public use, we can either not secure it, if it is fully public; that means no HTTPS, no Authorization header, no oAuth whatsoever. Or, we can use basic Authorization header + HTTPS, which is the most often-used one,  to secure the RESTful interaction between our trustworthy clients and our API.

Regarding that Servlet endpoint facilitating our internal admin to clear the cache, it should not have a public access at all. Further, even its internal API call should be secured programmatically, e.g. Spring security framework.

To wrap up, **NYCT-API** is a PoC solution to the coding challenge from DataRepublic. I've given my best to realise the minimum functional requirement within a short period of time. As such, a lot of improvement can be made on top of that. I'm looking forward to discussing that with you onsite :-).


# Referernce

> https://docs.spring.io/spring-boot/docs/current/maven-plugin/usage.html
> https://github.com/spring-cloud/spring-cloud-netflix/issues/106
> https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-reference-type-conversions.html
> https://mysqlserverteam.com/mysql-5-7-and-gis-an-example/